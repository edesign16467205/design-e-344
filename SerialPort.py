#!/usr/bin/env python
# -*- coding: latin-1 -*-
#
#    Copyright � 2008 Pierre Raybaut
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#    
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import serial, time
import doctest

# Set up the serial poort.  You only have to set the COM port correctly - the rest can stay the same
ser = serial.Serial('COM1',
                       baudrate=9600,
                       bytesize=serial.EIGHTBITS,
                       parity=serial.PARITY_NONE,
                       stopbits=serial.STOPBITS_ONE,
                       timeout=1,
                       xonxoff=0,
                       rtscts=0
                       )

def get_D(): 
	t1 = 0;

	while True:
		t1 = 1 + t1
		time.sleep(.5)
		input = ser.getCTS()
		
		if input == 0 :
			print "button off"
			
		if input == 1 :
			print "button on"
		if t1 >= 10 :
			return
	
			
		
def send_D():
	LED = raw_input('Ligt state?\n') 
	if LED == 'on':
		ser.setRTS(1)
	if LED == 'off':
		ser.setRTS(0)

def plus(a,b):
        c = a + b
        return c

a = int(raw_input('Integer 1\n'))
b = int(raw_input('Tnteger 2\n'))
c = plus(a,b)
print(c);

Cmd = raw_input('Send or Get data?\n')

while True:
	
	if Cmd == 'New_Cmd' :
		Cmd = raw_input('Send or Get data?\n')
	if Cmd == 'Send':
		send_D();
		Cmd = 'New_Cmd'
	if Cmd == 'Get':
		get_D();
		Cmd = 'New_Cmd'
	if Cmd == 'Stop':
		exit(); # exit

 
#Examples:

#set RTS line to specified logic level
#ser.setRTS(1)

#return the state of the CTS line
#ser.getCTS()

#Sleep for a second
#time.sleep(1)

