# Python program for heartbeat monitor.

# Import python files
import serial, time

# start GUI
from Tkinter import *

global port

# Class to create port selection frame
class port_GUI(Frame):
	
    def __init__(self, master):
        """ Initializes the Frame"""
        Frame.__init__(self,master)
        self.grid()
        self.create_widgets()
	
    def create_widgets(self):
        # Lable of Frame
        self.instruction = Label(self, text = "Enter Port#.")
        self.instruction.grid(row = 0, column = 0, columnspan = 1, sticky = W)

	# Entry to input PORT number used for heartbeat monitor
        self.port = Entry(self)
        self.port.grid(row = 0, column = 1, columnspan = 1, sticky = W)

        # Button to submit PORT number and exit Frame
        self.submit_button = Button(self, text = "Submit", command = self.reveal)
        self.submit_button.grid(row = 2, column = 0, sticky = W)

    # Initialise by Button
    def reveal(self):
        global port
        port = self.port.get()
        #Exit
        root.destroy()

# Class to create heartbeat monitor frame

def average(time1, x):
    """
    >>> average([1, 2, 3, 4, 5], 5)
    76.05

    >>> average([0.5, 1.08, 1.8, 2.4],4)
    129.31
    """
    bspttimedif = time1[x-1] - time1[0] - 0.011*x
    bps = (60/bspttimedif) * x
    r_avg = round(bps,2)
    return r_avg
    
def bps_average(avgbeats):
    """
    >>> bps_average([80.0, 75.0, 75.0, 80.0])
    77.5

    >>> bps_average([80.5, 75.25, 75.4, 80.3])
    77.86
    """
    r_avg2 = round(sum(avgbeats)/len(avgbeats),2)
    return r_avg2

# Setting to disable/Enable LED on circuit
def light_state(self):
    global light
    light = self.light.get()

class main_GUI(Frame):

    # Import Python files
    import serial, time
    import doctest

    # Global variables used
    global ser
    global avgbeats
    global light
    global average
    global bps_average
    global light
    
    def __init__(self, master):
        Frame.__init__(self,master)
        self.grid()
        self.create_main()

    def create_main(self):
        global port

        # Lable to display the PORT being used
        self.info = Label(self, text = port + ":")
        self.info.grid(row = 0, column = 0, columnspan = 1, sticky = W)

        # Button to start the heartbeat measuring 
        self.button_start = Button(self, text = "START", command = self.heart)
        self.button_start.grid(row = 1, column = 2, sticky = W)

        # Text to display average heartbeat
        self.beat = Text(self, width = 8, height = 1, wrap = WORD)
        self.beat.grid(row = 4, column = 1, columnspan = 1, sticky = W)

        # Label for the average heartbeat text block
        self.avg = Label(self, text = "Heartbeat:")
        self.avg.grid(row = 4, column = 0, columnspan = 1, sticky = W)

        # Lable for previous average hearbeats measured
        self.avg = Label(self, text = "Previous Heartbeats:")
        self.avg.grid(row = 4, column = 4, columnspan = 2, sticky = W)

        # Text to display previous average heartbeats measured
        self.beat1 = Text(self, width = 8, height = 10, wrap = WORD)
        self.beat1.grid(row = 5, column = 4, columnspan = 1, sticky = W)

        # Lable for average heartbeat of all heartbeats measured
        self.avg = Label(self, text = "Average Heartbeat:")
        self.avg.grid(row = 5, column = 0, columnspan = 1, sticky = W)

        # Text to display average heartbeat of all heartbeats measured
        self.beat2 = Text(self, width = 8, height = 1, wrap = WORD)
        self.beat2.grid(row = 5, column = 1, columnspan = 1, sticky = W)

        # Checkbutton to enable/disable LED on circuit(flash when user's heart beats) 
        self.light = BooleanVar()
        Checkbutton(self, text="Light", variable = self.light, command = self.light_state).grid(row = 6, column = 0, columnspan = 1, sticky =W)

    # Setting to disable/Enable LED on circuit
    def light_state(self):
        global light
        light = self.light.get()

    # Main function to calculate users heartbeat   
    def heart(self):
        
        global light
        t_start = 0
        stop =1

        # To ensure each on/off of hearbeat is only measured once
        var =1
        
        time1 = []
        bps = []
        
        i=0
        x=0
        s=1
        b=0

        # Wait to give time for user to get his figer on device
        time.sleep(3)
        
        a = 0

        #Average heartbeat over 14 heartbeats is measured
        while x < 14:

            # Low input (Heartbeat = High)
            if ser.getCTS() == 0 and var == 1:
                
                # Time when first beat is detected.
                time1.append(time.clock())
                
                var = 0;                

                # Increment heart beats measured
                x = x + 1
                
                s=0

                # LED on if checkbutton is checked
                if light == 1:
                    ser.setRTS(1)
                    
                # Acts as internal low pass filter to filter out false peaks
                time.sleep(0.5);

                # LED off if checkbutton is checked
                if light == 1 and x <13:
                    ser.setRTS(0)

                b=b+1    

            # High input (Heartbeat = Low)    
            if ser.getCTS() == 1 and var == 0:               
                var = 1;

            # Start displaying bps ons screan     
            if x >= 7 and s == 0:
            
                bps = average(time1, x)

                # Display heartbeat
                print bps
                print 'Average heart rate is {:.2f}bps over your last {} heartbeats.'.format(bps,x)
                
                s=1
        
        avgbeats.append(bps)
        ser.setRTS(1)
       
        r_avg2 = bps_average(avgbeats)
                

        
        self.beat2.delete(0.1,END)
        self.beat2.insert(0.1,r_avg2)
        
        self.beat.delete(0.1,END)
        self.beat.insert(0.1,bps)
        self.beat1.insert(0.1,bps)
        self.beat1.insert(0.1, "\n")
    
if __name__ == '__main__':
    import doctest
    ##import nosetests
    doctest.testmod()
    ##nosetests()
    global port

    # Run port selection frame and get port number used by the heartbeat monitor.
    root = Tk()
    root.title("Port Selection")
    root.geometry("250x50")
    port_num = port_GUI(root)
    root.mainloop()

    # PORT used by heartbeat monitor
    port = 'COM' + port

    # Serial port
    ser = serial.Serial( port,
                           baudrate=9600,
                           bytesize=serial.EIGHTBITS,
                           parity=serial.PARITY_NONE,
                           stopbits=serial.STOPBITS_ONE,
                           timeout=1,
                           xonxoff=0,
                            rtscts=0
                          )

    # Global variables
    avgbeats = []
    light = 0;

    root = Tk()
    root.title("Heartbeat monitor program")
    root.geometry("500x500")
    main_num = main_GUI(root)
    root.mainloop()


    
					   
#Examples:

#set RTS line to specified logic level
#ser.setRTS(1)

#return the state of the CTS line
#ser.getCTS()

#Sleep for a second
#time.sleep(1)
