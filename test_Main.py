
from MainV2 import light_state, port_GUI, bps_average, average, heart, main_GUI
	
 
from nose import with_setup #optional
from Tkinter import *

def my_setup_function():
    pass
 
def my_teardown_function():
    pass
 
@with_setup(my_setup_function, my_teardown_function)
def test_bps_average_integers():
    assert bps_average([80.0, 75.0, 75.0, 80.0]) == 77.5
def test_bps_average_floats():
    assert bps_average([80.5, 75.25, 75.4, 80.3]) == 77.86
def test_average_floats():
    assert average([0.5, 1.08, 1.8, 2.4],4) == 129.31
def test_light_state():
    assert False
def test_heart():
    pass
